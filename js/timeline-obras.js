var init = (function () {

	//VARIABLES
	var group1 = new TimelineMax({paused:true});
		//group2 = new TimelineMax({paused:true});
		group3 = new TimelineMax({paused:true});
		group4 = new TimelineMax({paused:true});
		group5 = new TimelineMax({paused:true});
		group6 = new TimelineMax({paused:true});
		group7 = new TimelineMax({paused:true});
		group8 = new TimelineMax({paused:true});
		group9 = new TimelineMax({paused:true});
		group10 = new TimelineMax({paused:true});
		group11 = new TimelineMax({paused:true});
	var player;

	//var place = $(".page").attr("data-section");

	//INICIAR ANIMACIONES


	new Waypoint({
	  element: document.getElementsByClassName('waypoint-1'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group1.play();
	    }else if(direction == 'up'){
	    	group1.reverse();
	    }
	  },
	  offset: '70%'
	});


	// new Waypoint({
	//   element: document.getElementsByClassName('waypoint-2'),
	//   handler: function(direction) {
	//     if(direction == 'down'){
	//     	group2.play();
	//     }else if(direction == 'up'){
	//     	group2.reverse();
	//     }
	//   },
	//   offset: '90%'
	// });

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-3'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group3.play();
	    }else if(direction == 'up'){
	    	group3.reverse();
	    }
	  },
	  offset: '75%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-3-1'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group4.play();
	    }else if(direction == 'up'){
	    	group4.reverse();
	    }
	  },
	  offset: '50%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-4'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group5.play();
	    }else if(direction == 'up'){
	    	group5.reverse();
	    }
	  },
	  offset: '75%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-4-1'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group6.play();
	    }else if(direction == 'up'){
	    	group6.reverse();
	    }
	  },
	  offset: '80%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-4-2'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group7.play();
	    }else if(direction == 'up'){
	    	group7.reverse();
	    }
	  },
	  offset: '80%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-4-3'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group8.play();
	    }else if(direction == 'up'){
	    	group8.reverse();
	    }
	  },
	  offset: '80%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-5'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group9.play();
	    }else if(direction == 'up'){
	    	group9.reverse();
	    }
	  },
	  offset: '80%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-6'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group10.play();
	    }else if(direction == 'up'){
	    	group10.reverse();
	    }
	  },
	  offset: '80%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-6-1'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group11.play();
	    }else if(direction == 'up'){
	    	group11.reverse();
	    }
	  },
	  offset: '50%'
	});


	//ANIMANDO
	
	group1
		.from(".waypoint-1 h2", 0.5, {opacity:0, x: -50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-1 p", 0.5, {opacity:0, y: 300, ease:Back.easeOut}, 0.2)
	// group2
	// 	.from(".waypoint-2", 0.5, {opacity:0, ease:Back.easeOut}, 0.1)
	// 	.from(".waypoint-2 .container", 1, {opacity:0, ease:Back.easeOut}, 0.5)
	group3
		.from(".waypoint-3 h2", 0.5, {opacity:0, x: -50, ease:Back.easeOut}, 0.1)
	group4
		.from("ul.waypoint-3-1 li.numb1", 0.5, {opacity:0, y: 100, ease:Back.easeOut}, 0.3)
		.from("ul.waypoint-3-1 li.numb2", 0.7, {opacity:0, y: 100, ease:Back.easeOut}, 0.5)
		.from("ul.waypoint-3-1 li.numb3", 0.8, {opacity:0, y: 100, ease:Back.easeOut}, 0.7)
		.from("ul.waypoint-3-1 li.numb4", 0.9, {opacity:0, y: 100, ease:Back.easeOut}, 0.9)
		.from("ul.waypoint-3-1 li.numb1 .tiempo", 0.5, {opacity:0, y: -300, ease:Back.easeOut}, 0.3)
		.from("ul.waypoint-3-1 li.numb2 .tiempo", 0.5, {opacity:0, y: -300, ease:Back.easeOut}, 0.5)
		.from("ul.waypoint-3-1 li.numb3 .tiempo", 0.5, {opacity:0, y: -300, ease:Back.easeOut}, 0.7)
		.from("ul.waypoint-3-1 li.numb4 .tiempo", 0.5, {opacity:0, y: -300, ease:Back.easeOut}, 0.9)
	group5
		.from(".waypoint-4 h2", 0.5, {opacity:0, x: -50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-4 .banner", 0.5, {opacity:0, delay: 0.6, ease:Back.easeOut}, 0.3)
	group6
		.from(".waypoint-4-1 .box.one", 0.5, {opacity:0, delay:1, x:-50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-4-1 .box.two", 0.5, {opacity:0, delay:1, x:-50, ease:Back.easeOut}, 0.4)
	group7
		.from(".waypoint-4-2 li.numb1", 0.5, {opacity:0, x:100, ease:Back.easeOut}, 0.1)
		.from(".waypoint-4-2 li.numb2", 0.5, {opacity:0, x:100, ease:Back.easeOut}, 0.2)
		.from(".waypoint-4-2 li.numb3", 0.5, {opacity:0, x:100, ease:Back.easeOut}, 0.3)
	group8
		.from(".waypoint-4-3 .one", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-4-3 .two", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.2)
	group9
		.from(".waypoint-5 h2", 0.5, {opacity:0, x: -50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-5 .left", 0.5, {opacity:0, x: -300, ease:Back.easeOut}, 0.5)
		.from(".waypoint-5 .right", 0.5, {opacity:0, x: 300, ease:Back.easeOut}, 0.6)
	group10
		.from(".waypoint-6 h3", 0.5, {opacity:0, x: 300, ease:Back.easeOut}, 0.1)
		.from(".waypoint-6 .fotos", 0.5, {opacity:0, delay:0.5, ease:Back.easeOut}, 0.3)
		//.from(".waypoint-6 .fotos", 0.5, {opacity:0, delay:0.5, ease:Back.easeOut}, 0.3)
	group11
		.from(".waypoint-6-1 .nav-left", 0.5, {opacity:0, x:-50, ease:Back.easeOut})
		.from(".waypoint-6-1 .nav-right", 0.5, {opacity:0, x:50, ease:Back.easeOut})
		













})();