var init = (function () {

	//VARIABLES
	var group1 = new TimelineMax({paused:true});
		group2 = new TimelineMax({paused:true});
		group3 = new TimelineMax({paused:true});
		group3_1 = new TimelineMax({paused:true});
		group4 = new TimelineMax({paused:true});
		group5 = new TimelineMax({paused:true});
		group6 = new TimelineMax({paused:true});
		group7 = new TimelineMax({paused:true});
		group8 = new TimelineMax({paused:true});
		group9 = new TimelineMax({paused:true});
		group10 = new TimelineMax({paused:true});
		group11 = new TimelineMax({paused:true});
	var player;

	//var place = $(".page").attr("data-section");

	//INICIAR ANIMACIONES


	new Waypoint({
	  element: document.getElementsByClassName('waypoint-1'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group1.play();
	    }else if(direction == 'up'){
	    	group1.reverse();
	    }
	  },
	  offset: '50%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-2'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group2.play();
	    }else if(direction == 'up'){
	    	group2.reverse();
	    }
	  },
	  offset: '70%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-3'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group3.play();
	    }else if(direction == 'up'){
	    	group3.reverse();
	    }
	  },
	  offset: '70%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-3-1'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group3_1.play();
	    }else if(direction == 'up'){
	    	group3_1.reverse();
	    }
	  },
	  offset: '80%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-4'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group4.play();
	    }else if(direction == 'up'){
	    	group4.reverse();
	    }
	  },
	  offset: '50%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-5'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group5.play();
	    }else if(direction == 'up'){
	    	group5.reverse();
	    }
	  },
	  offset: '80%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-6'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group6.play();
	    }else if(direction == 'up'){
	    	group6.reverse();
	    }
	  },
	  offset: '80%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-7'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group7.play();
	    }else if(direction == 'up'){
	    	group7.reverse();
	    }
	  },
	  offset: '80%'
	});



	//ANIMANDO
	
	group1
		.from(".waypoint-1 h2", 1, {opacity:0, y: -50, ease:Back.easeOut}, 0.1)
	group2
		.from(".waypoint-2 h2", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.1)	
		.from(".waypoint-2 p", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.3)
		.from(".waypoint-2 ul", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.4)
	group3
		.from(".waypoint-3 h2", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-3 .bquote", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.5)
		.from(".waypoint-3 p", 0.5, {opacity:0, ease:Back.easeOut}, 1)
	group3_1
		.from(".waypoint-3-1 .numb1", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-3-1 .numb2", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.3)
		.from(".waypoint-3-1 .numb3", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.5)
		.from(".waypoint-3-1 .numb4", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.7)
	group4
		.from(".waypoint-4 .subtitle", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-4 ul .numb1", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.3)
		.from(".waypoint-4 ul .numb2", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.5)
		.from(".waypoint-4 ul .numb3", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.7)
	group5
		.from(".waypoint-5 h3", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-5 p", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.2)
		.from(".waypoint-5 ul", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.5)
	group6
		.from(".waypoint-6 .left", 0.5, {opacity:0}, 0.1)
		.from(".waypoint-6 h2", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.1)
	group7
		.from(".waypoint-7 img", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.1)





	$("header .btn-menu-open").on("click", function(){
		$("header nav").addClass('nav-active');
	});

	$("header nav .btn-menu-close").on("click", function(){
		$("header nav").removeClass('nav-active');
		//$('header').removeClass('show');
	});

	//NAV FIXED
	var delay = 600;
	var timeout = null;

	$(window).on('scroll', function() {

		var navHeight = $('header').height();

		clearTimeout(timeout);

		if ($(window).scrollTop() > navHeight) {
			$('header').addClass('fixed');
			
			$('header').removeClass("hidden");

			$('header').on('mouseover', function(){
				$('header').addClass('show');
			});
			
			$('header').on('mouseout', function(){
				$('header').removeClass('show');
			});
	
			timeout = setTimeout(function(){
				$('header').addClass("hidden");
			},delay);
		}
		else {
			$('header').removeClass('fixed');
			//$('header').css("visibility", "visible");
			$('header').removeClass("hidden");
			$('.fixed').removeClass("show");
		}	

	});



})();