$(document).ready(function(){

	//NAV
	$('nav a.becas').on('click', function(){
		ga('send','event','Menu','clic','becas');
	});
	$('nav a.eduFinanciera').on('click', function(){
		ga('send','event','Menu','clic','Ed_financiera');
	});
	$('nav a.obras').on('click', function(){
		//ga('send','event','Menu','clic','OXI');
		ga('send','event','Menu','clic','Obras');
	});
	$('nav a.reporting').on('click', function(){
		ga('send','event','Menu','clic','Reporting');
	});
	$('nav a.lineTime').on('click', function(){
		ga('send','event','Menu','clic','Linea');
	});

	//FOOTER
	$('footer .menu-1 a.becas').on('click', function(){
		ga('send','event','Footer','clic','Becas');
	});
	$('footer .menu-1 a.eduFinanciera').on('click', function(){
		ga('send','event','Footer','clic','Ed_financiera');
	});
	$('footer .menu-1 a.obras').on('click', function(){
		ga('send','event','Foooter','clic','Obras');
	});
	$('footer .menu-1 a.reporting').on('click', function(){
		ga('send','event','Footer','clic','reporting');
	});
	$('footer .menu-1 a.Ltiempo').on('click', function(){
		ga('send','event','Footer','clic','Linea');
	});

	//FOOTER REDES
	$('footer u.social .facebook').on('click', function(){
		ga('send','event','Footer','clic','facebook');
	});
	$('footer u.social .twitter').on('click', function(){
		ga('send','event','Footer','clic','twitter');
	});

	//LÍNEA DE TIEMPO
	$('.nav-line a.tab1').on('click', function(){
		ga('send','event','Linea de tiempo','clic','1962');
	});
	$('.nav-line a.tab2').on('click', function(){
		ga('send','event','Linea de tiempo','clic','1990');
	});
	$('.nav-line a.tab3').on('click', function(){
		ga('send','event','Linea de tiempo','clic','2002');
	});
	$('.nav-line a.tab4').on('click', function(){
		ga('send','event','Linea de tiempo','clic','2009');
	});
	$('.nav-line a.tab5').on('click', function(){
		ga('send','event','Linea de tiempo','clic','2010');
	});
	$('.nav-line a.tab6').on('click', function(){
		ga('send','event','Linea de tiempo','clic','2014');
	});


});