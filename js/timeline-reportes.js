var init = (function () {

	//VARIABLES
	var group1 = new TimelineMax({paused:true});
		group2 = new TimelineMax({paused:true});
		group3 = new TimelineMax({paused:true});
		group4 = new TimelineMax({paused:true});
		group5 = new TimelineMax({paused:true});
		group6 = new TimelineMax({paused:true});
		group7 = new TimelineMax({paused:true});
		group8 = new TimelineMax({paused:true});
		group9 = new TimelineMax({paused:true});
		group10 = new TimelineMax({paused:true});
		group11 = new TimelineMax({paused:true});
	var player;

	//var place = $(".page").attr("data-section");

	//INICIAR ANIMACIONES


	new Waypoint({
	  element: document.getElementsByClassName('waypoint-1'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group1.play();
	    }else if(direction == 'up'){
	    	group1.reverse();
	    }
	  },
	  offset: '80%'
	});
	new Waypoint({
	  element: document.getElementsByClassName('waypoint-2'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group2.play();
	    }else if(direction == 'up'){
	    	group2.reverse();
	    }
	  },
	  offset: '70%'
	});
	new Waypoint({
	  element: document.getElementsByClassName('waypoint-3'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group3.play();
	    }else if(direction == 'up'){
	    	group3.reverse();
	    }
	  },
	  offset: '70%'
	});
	new Waypoint({
	  element: document.getElementsByClassName('waypoint-4'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group4.play();
	    }else if(direction == 'up'){
	    	group4.reverse();
	    }
	  },
	  offset: '70%'
	});
	new Waypoint({
	  element: document.getElementsByClassName('waypoint-5'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group5.play();
	    }else if(direction == 'up'){
	    	group5.reverse();
	    }
	  },
	  offset: '70%'
	});
	new Waypoint({
	  element: document.getElementsByClassName('waypoint-6'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group6.play();
	    }else if(direction == 'up'){
	    	group6.reverse();
	    }
	  },
	  offset: '30%'
	});
	new Waypoint({
	  element: document.getElementsByClassName('waypoint-7'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group7.play();
	    }else if(direction == 'up'){
	    	group7.reverse();
	    }
	  },
	  offset: '70%'
	});
	new Waypoint({
	  element: document.getElementsByClassName('waypoint-8'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group8.play();
	    }else if(direction == 'up'){
	    	group8.reverse();
	    }
	  },
	  offset: '70%'
	});

	//ANIMANDO
	
	group1
		.from(".waypoint-1 h2", 0.5, {opacity:0, x: -50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-1 p", 0.5, {opacity:0, y: 300, ease:Back.easeOut}, 0.2)
		.from(".waypoint-1 .bquote", 0.5, {opacity:0, y: 300, ease:Back.easeOut}, 0.5)
	group2
		.from(".waypoint-2 .left > span", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-2 .left img", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.3)
		.from(".waypoint-2 .right ul li", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.5)
	group3
		.from(".waypoint-3 .left > span", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-3 .left img", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.3)
		.from(".waypoint-3 .right ul li", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.5)
	group4
		.from(".waypoint-4 .left > span", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-4 .left img", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.3)
		.from(".waypoint-4 .right ul li", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.5)
	group5
		.from(".waypoint-5 .left > span", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-5 .left img", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.3)
		.from(".waypoint-5 .right ul li", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.5)
	group6
		.from(".waypoint-6 .container div", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.1)
	group7
		.from(".waypoint-7 .left > span", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-7 .left img", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.3)
		.from(".waypoint-7 .right ul li", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.5)
	group8
		.from(".waypoint-8 .left > span", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-8 .left img", 0.5, {opacity:0, y:50, ease:Back.easeOut}, 0.3)
		.from(".waypoint-8 .right ul li", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.5)
	
		













})();