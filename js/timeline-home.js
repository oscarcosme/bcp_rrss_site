var init = (function () {

	//VARIABLES
	var group1 = new TimelineMax({paused:true});
		group2 = new TimelineMax({paused:true});
		group3 = new TimelineMax({paused:true});
		group3_1 = new TimelineMax({paused:true});
		group3_2 = new TimelineMax({paused:true});
		group3_3 = new TimelineMax({paused:true});
		group4 = new TimelineMax({paused:true});
	var player;

	//var place = $(".page").attr("data-section");

	//INICIAR ANIMACIONES

	//Destruimos la animación para mobile
	var isMobile = window.matchMedia("screen and (max-width: 760px)");

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-1'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group2.play();
	    }else if(direction == 'up'){
	    	group2.reverse();
	    }
	    if (isMobile.matches) {
	        this.destroy();
	    }
	  },
	  offset: '80%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-2'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group2.play();
	    }else if(direction == 'up'){
	    	group2.reverse();
	    }
	    if (isMobile.matches) {
	        this.destroy();
	    }
	  },
	  offset: '80%'
	});

	new Waypoint({
	  element: document.getElementsByClassName('waypoint-3'),
	  handler: function(direction) {
	    if(direction == 'down'){
	    	group3.play();
	    }else if(direction == 'up'){
	    	group3.reverse();
	    }
	    if (isMobile.matches) {
	        this.destroy();
	    }
	  },
	  offset: '70%'
	});
	if (screen.width > 768) {
		new Waypoint({
		  element: document.getElementsByClassName('waypoint-4'),
		  handler: function(direction) {
		    if(direction == 'down'){
		    	group4.play();
		    }else if(direction == 'up'){
		    	group4.reverse();
		    }
		    if (isMobile.matches) {
		        this.destroy();
		    }
		  },
		  offset: '50%'
		});
	} else {
		new Waypoint({
		  element: document.getElementsByClassName('waypoint-4'),
		  handler: function(direction) {
		    if(direction == 'down'){
		    	group4.play();
		    }else if(direction == 'up'){
		    	group4.reverse();
		    }
		    if (isMobile.matches) {
		        this.destroy();
		    }
		  },
		  offset: '70%'
		});
	}

	//ANIMANDO
	
	group2
		.from(".home-scholarships-credits", 1, { opacity:0, delay: 0.1, ease:Cubic.easeOut })
		.from(".waypoint-2", 1, { opacity:0, delay: 0.1, ease:Cubic.easeOut })
		.from(".waypoint-2 .bgs-pos.left", 0.3, {left: '-100%',}, 2.1)
		.from(".waypoint-2 .bgs-pos.right", 0.3, {right: '-100%', ease: Cubic.easeOut}, 2)
		.from(".waypoint-2 .content-bg.left img", 0.5, {opacity:0, x:-200, ease:Back.easeOut}, 2.6)
		.from(".waypoint-2 .content-bg.right img", 0.5, {opacity:0, x:200, ease:Back.easeOut}, 2.7)
		.from(".waypoint-2 .content-bg.left p", 0.5, {opacity:0, y: 200, ease:Back.easeOut}, 2.8)
		.from(".waypoint-2 .content-bg.right p", 0.5, {opacity:0, y: 200, ease:Back.easeOut}, 2.9)
		.from(".waypoint-2 a", 1, {opacity:0, y: 200, ease:Back.easeOut}, 3)

	group3
		.from(".waypoint-3", 0.5, {opacity:0, delay: 0.1, ease:Cubic.easeOut}, 0.1)
		.from(".waypoint-3 .bg_02", 1, {opacity:0, delay: 0.1}, 0.4)
		.from(".waypoint-3 h1", 0.5, {opacity:0, y:-200, ease:Back.easeOut}, 0.5)
		.from(".waypoint-3 p", 0.5, {opacity:0, y:200, ease:Back.easeOut}, 0.7)

	// group3_1
	// 	.to(".waypoint-3 .bg_02", 0.5, {opacity:0, delay:2}, 0.1)
	// 	.from(".waypoint-3-1 .bg_03", 1, {opacity:0, delay:2}, 0.5)

	// group3_2
	// 	.to(".waypoint-3-1 .bg_03", 0.5, {opacity:0, display:'block', delay:2}, 0.1)
	// 	.from(".waypoint-3-2 .bg_04", 1, {opacity:0, delay:2}, 0.5)

	// group3_3
	// 	.to(".waypoint-3-2 .bg_04", 0.5, {opacity:0, delay:2}, 0.1)
	// 	.from(".waypoint-3-3 .bg_05", 1, {opacity:0, delay:2}, 0.5)


	group4
		.from(".waypoint-4", 0.5, {opacity:0, delay: 0.1, ease:Cubic.easeOut}, 0.1)
		.from(".waypoint-4 .left", 0.5, {opacity:0, ease:Back.easeOut}, 0.4)
		.from(".waypoint-4 .right", 0.5, {opacity:0, ease:Back.easeOut}, 0.5)
		.from(".waypoint-4 a.more", 0.5, {opacity:0, top: 100, ease:Back.easeOut}, 0.5)
		.from(".waypoint-4 .left h2", 0.5, {opacity:0, x:-200, ease:Back.easeOut}, 0.6)
		.from(".waypoint-4 .right h2", 0.5, {opacity:0, x:200, ease:Back.easeOut}, 0.6)













})();