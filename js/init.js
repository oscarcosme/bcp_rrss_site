/**
* @description: Standard Layout
* @author: Oscar Cosme
* @date: 22/09/15
* @version: 1.0
*/

$(function() {

	//NAV FOR IE
	$('.btn-menu-open').on('click', function(){
		$('header nav').addClass('show-nav');
	});
	$('.btn-menu-close').on('click', function(){
		$('header nav').removeClass('show-nav');
	});
	//

	$(".home-banner .carousel").carouFredSel({
		auto: true,
		responsive: true,
		height : "554px",
		//height : "auto",
		width : "100%",
		scroll : { 
			fx : "crossfade",
			duration : 1500
		},
		//duration : 1500,
		pagination: ".home-banner .pager"
	});
	$(".slider-two .carousel").carouFredSel({
		auto: true,
		responsive: true,
		// height : "554px",
		height : "595px",
		width : "100%",
		scroll : { 
			fx : "crossfade",
			duration : 1500
		},
		//duration : 1500,
		pagination: ".slider-two .pager"
	});
	$(".section-scholarships .scholarships-gallery .carousel").carouFredSel({
		auto: false,
		transition: false,
		responsive: true,
		height : "382px",
		width : "100%",
		scroll : {
			fx : "crossfade",
			duration : 1000
		},

		pagination: ".section-scholarships .scholarships-gallery .pager"
	});
	$(".section-credits .scholarships-gallery .carousel").carouFredSel({
		auto: false,
		transition: false,
		responsive: true,
		height : "382px",
		width : "100%",
		scroll : {
			fx : "crossfade",
			duration : 1000
		},

		pagination: ".section-credits .scholarships-gallery .pager"
	});
	//GALLERY FOR MOBILE
	$(".section-scholarships .scholarships-fotos .fotos-for-mobile .carousel").carouFredSel({
		auto: true,
		//transition: false,
		responsive: true,
		height : "240px",
		width : "100%",
		scroll : {
			fx : "crossfade",
			duration : 1000
		},

		pagination: ".scholarships-fotos .fotos-for-mobile .pager"
	});


	$("header .btn-menu-open").on("click", function(){
		$("header nav").addClass('nav-active');
	});

	$("header nav .btn-menu-close").on("click", function(){
		$("header nav").removeClass('nav-active');
		//$('header').removeClass('show');
	});

	//NAV FIXED
	var delay = 600;
	var timeout = null;

	$(window).on('scroll', function() {

		var navHeight = $('header').height();

		clearTimeout(timeout);

		if ($(window).scrollTop() > navHeight) {
			$('header').addClass('fixed');
			
			$('header').removeClass("hidden");

			$('header').on('mouseover', function(){
				$('header').addClass('show');
			});
			
			$('header').on('mouseout', function(){
				$('header').removeClass('show');
			});
	
			timeout = setTimeout(function(){
				$('header').addClass("hidden");
			},delay);
		}
		else {
			$('header').removeClass('fixed');
			//$('header').css("visibility", "visible");
			$('header').removeClass("hidden");
			$('.fixed').removeClass("show");
		}	
	});


	//
	var place = $(".page").attr("data-section");

	if(place == 'becasCreditos'){
		$('.menu li a.becas').addClass('on');
	}else if(place == 'obrasImpuestos') {
		$('.menu li a.obras').addClass('on');
	}

});

$(document).ready(function(){
	//ICON DOWN
	$('a.icon-down').on("click", function(e){
		e.preventDefault();
		$('.section-universities').fadeIn(500);
	});
	$('span.icon-down2 img').on("click", function(){
		$('.section-universities').fadeOut(500);
		// $('span.icon-down').css('display', 'block');
	});
	//REPORTING
	$('a.show').on("click", function(e){
		e.preventDefault();
		$('.scholarships-gallery.files').fadeIn(500);

		$(".scholarships-gallery.files .carousel").carouFredSel({
			auto: false,
			transition: false,
			responsive: true,
			height : "382px",
			width : "100%",
			scroll : {
				fx : "crossfade",
				duration : 1000
			},

			pagination: ".scholarships-gallery.files .pager"
		});
	});
	$('a.show2').on("click", function(e){
		e.preventDefault();
		$('.scholarships-gallery.principios').fadeIn(500);

		$(".scholarships-gallery.principios .carousel2").carouFredSel({
			auto: false,
			transition: false,
			responsive: true,
			height : "382px",
			width : "100%",
			scroll : {
				fx : "crossfade",
				duration : 1000
			},

			pagination: ".scholarships-gallery.principios .pager"
		});
	});


	//

	//$('section.home-scholarships-credits-info a.go_credits').on('click', function(e){
		// e.preventDefault;
		// var link = $(this).attr('href')
		// alert('redireccionando');
		// location.href = link;

		// var place = $(".page").attr("data-section");

		// if(place == 'becasCreditos'){
		// 	alert('Estamos en creditos');
		// 	//$('.menu li a.becas').addClass('on');
		// 	$('ul.menu-tabs li.one').removeClass('on');
		// 	$('ul.menu-tabs li.two').addClass('on');

		// 	$('.section-scholarships').removeClass('active');
		// 	$('.section-credits').addClass('active');
		// }
	//})

	var link = $(this).attr('href');

	$('section.home-scholarships-credits-info a.go_credits').on('click', function(){

		var link = $(this).attr('href');
		location.href = link;

	})
	
	if(location.href.indexOf("type=credito") != -1) {

		//alert("tu url contiene type=credito");

		$('ul.menu-tabs li.one').removeClass('on');
	 	$('ul.menu-tabs li.two').addClass('on');
	 	
	 	$('li.two .arrow').show();
		$('li.one .arrow').hide();

		$('.section-scholarships').removeClass('active');
		$('.section-credits').addClass('active');

	}
	
	//For Mobile - scholarships-as-elaborated
	$('ul.displayTab li a').on('click', function(e){
		e.preventDefault();

		var data = $(this).attr('data-item');
		//alert(data);
		$(this).toggleClass('active');
		$('ul.displayTab li .data-item-'+data).toggleClass('show');
	});

	//GALLERY MOBILE

	// var pswpElement = document.querySelectorAll('.my-gallery')[0];

	// Initializes and opens PhotoSwipe
	// var gallery = new PhotoSwipe( pswpElement );
	
	// gallery.init();


	//LINKS DEL HOME => BECAS
	$('.home-scholarships-credits-info img.respo').on('click', function(){
		location.href = 'http://responsabilidadsocialbcp.com/becas-y-creditos.html';
	})


	//GALLERY
	var initPhotoSwipeFromDOM = function(gallerySelector) {

	    // parse slide data (url, title, size ...) from DOM elements 
	    // (children of gallerySelector)
	    var parseThumbnailElements = function(el) {
	        var thumbElements = el.childNodes,
	            numNodes = thumbElements.length,
	            items = [],
	            figureEl,
	            linkEl,
	            size,
	            item;

	        for(var i = 0; i < numNodes; i++) {

	            figureEl = thumbElements[i]; // <figure> element

	            // include only element nodes 
	            if(figureEl.nodeType !== 1) {
	                continue;
	            }

	            linkEl = figureEl.children[0]; // <a> element

	            size = linkEl.getAttribute('data-size').split('x');

	            // create slide object
	            item = {
	                src: linkEl.getAttribute('href'),
	                w: parseInt(size[0], 10),
	                h: parseInt(size[1], 10)
	            };



	            if(figureEl.children.length > 1) {
	                // <figcaption> content
	                item.title = figureEl.children[1].innerHTML; 
	            }

	            if(linkEl.children.length > 0) {
	                // <img> thumbnail element, retrieving thumbnail url
	                item.msrc = linkEl.children[0].getAttribute('src');
	            } 

	            item.el = figureEl; // save link to element for getThumbBoundsFn
	            items.push(item);
	        }

	        return items;
	    };

	    // find nearest parent element
	    var closest = function closest(el, fn) {
	        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
	    };

	    // triggers when user clicks on thumbnail
	    var onThumbnailsClick = function(e) {
	        e = e || window.event;
	        e.preventDefault ? e.preventDefault() : e.returnValue = false;

	        var eTarget = e.target || e.srcElement;

	        // find root element of slide
	        var clickedListItem = closest(eTarget, function(el) {
	            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
	        });

	        if(!clickedListItem) {
	            return;
	        }

	        // find index of clicked item by looping through all child nodes
	        // alternatively, you may define index via data- attribute
	        var clickedGallery = clickedListItem.parentNode,
	            childNodes = clickedListItem.parentNode.childNodes,
	            numChildNodes = childNodes.length,
	            nodeIndex = 0,
	            index;

	        for (var i = 0; i < numChildNodes; i++) {
	            if(childNodes[i].nodeType !== 1) { 
	                continue; 
	            }

	            if(childNodes[i] === clickedListItem) {
	                index = nodeIndex;
	                break;
	            }
	            nodeIndex++;
	        }



	        if(index >= 0) {
	            // open PhotoSwipe if valid index found
	            openPhotoSwipe( index, clickedGallery );
	        }
	        return false;
	    };

	    // parse picture index and gallery index from URL (#&pid=1&gid=2)
	    var photoswipeParseHash = function() {
	        var hash = window.location.hash.substring(1),
	        params = {};

	        if(hash.length < 5) {
	            return params;
	        }

	        var vars = hash.split('&');
	        for (var i = 0; i < vars.length; i++) {
	            if(!vars[i]) {
	                continue;
	            }
	            var pair = vars[i].split('=');  
	            if(pair.length < 2) {
	                continue;
	            }           
	            params[pair[0]] = pair[1];
	        }

	        if(params.gid) {
	            params.gid = parseInt(params.gid, 10);
	        }

	        return params;
	    };

	    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
	        var pswpElement = document.querySelectorAll('.pswp')[0],
	            gallery,
	            options,
	            items;

	        items = parseThumbnailElements(galleryElement);

	        // define options (if needed)
	        options = {

	            // define gallery index (for URL)
	            galleryUID: galleryElement.getAttribute('data-pswp-uid'),

	            getThumbBoundsFn: function(index) {
	                // See Options -> getThumbBoundsFn section of documentation for more info
	                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
	                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
	                    rect = thumbnail.getBoundingClientRect(); 

	                return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
	            }

	        };

	        // PhotoSwipe opened from URL
	        if(fromURL) {
	            if(options.galleryPIDs) {
	                // parse real index when custom PIDs are used 
	                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
	                for(var j = 0; j < items.length; j++) {
	                    if(items[j].pid == index) {
	                        options.index = j;
	                        break;
	                    }
	                }
	            } else {
	                // in URL indexes start from 1
	                options.index = parseInt(index, 10) - 1;
	            }
	        } else {
	            options.index = parseInt(index, 10);
	        }

	        // exit if index not found
	        if( isNaN(options.index) ) {
	            return;
	        }

	        if(disableAnimation) {
	            options.showAnimationDuration = 0;
	        }

	        // Pass data to PhotoSwipe and initialize it
	        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
	        gallery.init();
	    };

	    // loop through all gallery elements and bind events
	    var galleryElements = document.querySelectorAll( gallerySelector );

	    for(var i = 0, l = galleryElements.length; i < l; i++) {
	        galleryElements[i].setAttribute('data-pswp-uid', i+1);
	        galleryElements[i].onclick = onThumbnailsClick;
	    }

	    // Parse URL and open gallery if it contains #&pid=3&gid=1
	    var hashData = photoswipeParseHash();
	    if(hashData.pid && hashData.gid) {
	        openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
	    }
	};

	// execute above function
	initPhotoSwipeFromDOM('.my-gallery');



	/*========================
	LIGHTBOX CONTACTO
	==========================*/

	var contacto = $('#box-contacto');

	$('a.contacto').on('click', function(e){
		e.preventDefault();
		
		contacto.fadeIn('slow');


		jQuery.fn.center = function(parent) {
		    if (parent) {
		        parent = this.parent();
		    } else {
		        parent = window;
		    }
		    this.css({
		        "position": "absolute",
		        "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
		        "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
		    });
		return this;
		}
		$('.mensaje').center(true);
		//$("div.target:nth-child(2)").center(false);

		$("body").css("overflow", "hidden");
		
	});
	
	$('#box-contacto .mensaje .contenedor img').on('click', function(){
		//contacto.removeClass('show');
		contacto.hide();
		$("body").css("overflow", "initial");
	})


	// <!-- Link desde el home

	$('.cartilla a.btn-more').on('click', function(){

		var cartilla = $(this).attr('href');
		location.href = cartilla;

	})
	// -->

	$('#box-cartilla .mensaje .contenedor img').on('click', function(){
		//contacto.removeClass('show');
		contacto.hide();
		$("body").css("overflow", "initial");
	})


	//EDU FINANCIERA
	$('.scholarships-credits-header.edu-financiera h3 a').on('click', function(e){

		var cartilla = $('#box-cartilla');
			
		cartilla.fadeIn('slow');


		jQuery.fn.center = function(parent) {
		    if (parent) {
		        parent = this.parent();
		    } else {
		        parent = window;
		    }
		    this.css({
		        "position": "absolute",
		        "top": ((($(parent).height() - this.outerHeight()) / 8) + $(parent).scrollTop() + "px"),
		        "left": ((($(parent).width() - this.outerWidth()) / 5) + $(parent).scrollLeft() + "px")
		    });

			return this;
		}
		
		$('.mensaje').center(true);

		$("body").css("overflow", "hidden");
			
		
		$('#box-cartilla .mensaje .contenedor img').on('click', function(){
			//contacto.removeClass('show');
			cartilla.hide();
			$("body").css("overflow", "initial");
		})

		e.preventDefault();
	})



});



