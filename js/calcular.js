$(document).ready(function(){



	$.fn.digits = function(){ 
		return this.each(function(){ 
			$(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
		})
	}

	//CALCULADORA DE CRÉDITO

	//Vars Global
	var var1 = 6; //Periodo de gracia
	var reciPeriodica = 2; //Envío de Inf. Periódica	|| 1 or 2
	var periodos  = 60; //Equivalente a los 5 años
	//var costoEnvioRecibo;


	$('.section-credito-estudios a').on('click', function(e){
		e.preventDefault();
		$('.section-calcular').show(300);

	});

	var totalFinanciar;
	var cuotaMensual;
	var ciclos;
	var montoCiclo;

	$('a.btn-calcular').on('click', function(e){
				
		formCalc = $('#datos_calc');
		ciclos = $('#ciclo').val();
		montoCiclo = $('#monto').val();

		//Total a financiar
		//var totalFinanciar = ciclos * montoCiclo;

		if (formCalc.valid() == true ) {
			console.log('entro');
			if(ciclos <= 9) {
				totalFinanciar = ciclos * montoCiclo;

				if (reciPeriodica == 1) {

				 	var costoEnvioRecibo = 0;

				}else{

					var costoEnvioRecibo = 10;

				};

				//Formula tasa mensual
				var tasaMensual = + Math.pow( (1 + 4/100) , (1/12) ) -1;
				


				//Operación
				cuotaMensual = (totalFinanciar * tasaMensual) / ( 1 - (1 / Math.pow(1 + tasaMensual,periodos) ));
				

				//alert(totalFinanciar);
				$('.section-calcular .results span.two').text("S/ " + totalFinanciar.toFixed(2)).digits();
				//$('.section-calcular .results span.four').text("S/ " + cuotaMensual.toPrecision(5));
				$('.section-calcular .results span.four').text("S/ " + cuotaMensual.toFixed(2));
				

				//Display box results
				$('.section-calcular .results').show();
				$('.section-calcular .download').show();

			}else {
				$('input#ciclo').addClass('error');
				//alert('número de cuotas max 9');
			}
		}

		e.preventDefault();


	});


	//GENERAR PDF

	$('.section-calcular #descargar').on('click', function(e){
		e.preventDefault();


		//window.open("http://localhost/bcp_rrss_site/generate.php?v=1000&p=2000");

		window.open("generate.php?v="+totalFinanciar+"&p="+cuotaMensual.toFixed(2)+"&a="+ciclos+"&b="+montoCiclo)
;
	});









});
	