$(document).ready(function(){
//var init = (function () {

	//VARIABLES
	var group1 = new TimelineMax({paused:true});
		group2 = new TimelineMax({paused:true});
		group3 = new TimelineMax({paused:true});
		group4 = new TimelineMax({paused:true});
		group5 = new TimelineMax({paused:true});
		group6 = new TimelineMax({paused:true});
		group7 = new TimelineMax({paused:true});
		group8 = new TimelineMax({paused:true});
		//group9 = new TimelineMax({paused:true});
		//group10 = new TimelineMax({paused:true});
		group11 = new TimelineMax({paused:true});
		group12 = new TimelineMax({paused:true});
		group13 = new TimelineMax({paused:true});
		//group14 = new TimelineMax({paused:true});
		group15 = new TimelineMax({paused:true});



	var player;

	function iniciar(tab){

		switch(tab){
			case 'finanzas':

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-1'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group1.play();
				    }else if(direction == 'up'){
				    	group1.reverse();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-2'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group2.play();
				    }else if(direction == 'up'){
				    	group2.reverse();
				    }
				  },
				  offset: '50%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-3'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group3.play();
				    }else if(direction == 'up'){
				    	group3.reverse();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-4'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group4.play();
				    }else if(direction == 'up'){
				    	group4.reverse();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-5'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group5.play();
				    }else if(direction == 'up'){
				    	group5.reverse();
				    }
				  },
				  offset: '70%'
				});

				break;

			case 'agentes':

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-6'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group6.play();
				    }else if(direction == 'up'){
				    	group6.reverse();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-7'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group7.play();
				    }else if(direction == 'up'){
				    	group7.reverse();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-8'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group8.play();
				    }else if(direction == 'up'){
				    	group8.reverse();
				    }
				  },
				  offset: '70%'
				});

				// new Waypoint({
				//   element: document.getElementsByClassName('waypoint-9'),
				//   handler: function(direction) {
				//     if(direction == 'down'){
				//     	group9.play();
				//     }else if(direction == 'up'){
				//     	group9.reverse();
				//     }
				//   },
				//   offset: '70%'
				// });

				// new Waypoint({
				//   element: document.getElementsByClassName('waypoint-10'),
				//   handler: function(direction) {
				//     if(direction == 'down'){
				//     	group10.play();
				//     }else if(direction == 'up'){
				//     	group10.reverse();
				//     }
				//   },
				//   offset: '70%'
				// });

				break;

			case 'universitaria':

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-11'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group11.play();
				    }else if(direction == 'up'){
				    	group11.reverse();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-12'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group12.play();
				    }else if(direction == 'up'){
				    	group12.reverse();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-13'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group13.play();
				    }else if(direction == 'up'){
				    	group13.reverse();
				    }
				  },
				  offset: '70%'
				});

				// new Waypoint({
				//   element: document.getElementsByClassName('waypoint-14'),
				//   handler: function(direction) {
				//     if(direction == 'down'){
				//     	group14.play();
				//     }else if(direction == 'up'){
				//     	group13.reverse();
				//     }
				//   },
				//   offset: '70%'
				// });

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-15'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group15.play();
				    }else if(direction == 'up'){
				    	group15.reverse();
				    }
				  },
				  offset: '70%'
				});

				break;
		}

	}

	//ANIMANDO

	//TAB 1
	group1
		.from(".waypoint-1 h2", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-1 p", 0.5, {y:100, opacity:0, ease:Back.easeOut}, 0.3)
	group2
		.from(".waypoint-2 h2", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-2 ul.etapas li.numb1", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-2 ul.etapas li.numb1 .image", 0.5, {y:-100, opacity:0, ease:Back.easeOut}, 0.2)
		.from(".waypoint-2 ul.etapas li.numb1 .ul_text", 0.5, {y:100, opacity:0, ease:Back.easeOut}, 0.2)
		.from(".waypoint-2 ul.etapas li.numb2", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 0.4)
		.from(".waypoint-2 ul.etapas li.numb2 .image", 0.5, {y:-100, opacity:0, ease:Back.easeOut}, 0.5)
		.from(".waypoint-2 ul.etapas li.numb2 .ul_text", 0.5, {y:100, opacity:0, ease:Back.easeOut}, 0.5)
		.from(".waypoint-2 ul.etapas li.numb3", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 0.7)
		.from(".waypoint-2 ul.etapas li.numb3 .image", 0.5, {y:-100, opacity:0, ease:Back.easeOut}, 0.8)
		.from(".waypoint-2 ul.etapas li.numb3 .ul_text", 0.5, {y:100, opacity:0, ease:Back.easeOut}, 0.8)
	group3
		.from(".waypoint-3 h2", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-3 .franja", 0.5, {opacity:0}, 0.1)
		.from(".waypoint-3 .franja ul li.numb1", 0.5, {x:50, opacity:0, ease:Back.easeOut}, 0.5)
		.from(".waypoint-3 .franja ul li.numb2", 0.5, {x:50, opacity:0, ease:Back.easeOut}, 0.7)
		.from(".waypoint-3 .franja ul li.numb3", 0.5, {x:50, opacity:0, ease:Back.easeOut}, 0.9)
		.from(".waypoint-3 .franja ul li.numb4", 0.5, {x:50, opacity:0, ease:Back.easeOut}, 1.1)
	group4
		.from(".waypoint-4 h2", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-4 .left", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 0.5)
		.from(".waypoint-4 .right", 0.5, {x:100, opacity:0, ease:Back.easeOut}, 0.6)
	group5
		.from(".waypoint-5 h3", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-5 ul", 0.5, {opacity:0, delay:0.7}, 0.1)
		.from(".waypoint-5 .nav-left", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.5)
		.from(".waypoint-5 .nav-right", 0.5, {x:50, opacity:0, ease:Back.easeOut}, 0.6)
	//TAB 2
	group6
		.from(".waypoint-6 h2", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-6 p", 0.5, {y:100, opacity:0, ease:Back.easeOut}, 0.3)
	group7
		.from(".waypoint-7 h2", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-7 ul.etapas li.numb1", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-7 ul.etapas li.numb1 .image", 0.5, {y:-100, opacity:0, ease:Back.easeOut}, 0.2)
		.from(".waypoint-7 ul.etapas li.numb1 .ul_text", 0.5, {y:100, opacity:0, ease:Back.easeOut}, 0.2)
		.from(".waypoint-7 ul.etapas li.numb2", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 0.4)
		.from(".waypoint-7 ul.etapas li.numb2 .image", 0.5, {y:-100, opacity:0, ease:Back.easeOut}, 0.5)
		.from(".waypoint-7 ul.etapas li.numb2 .ul_text", 0.5, {y:100, opacity:0, ease:Back.easeOut}, 0.5)
		.from(".waypoint-7 ul.etapas li.numb3", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 0.7)
		.from(".waypoint-7 ul.etapas li.numb3 .image", 0.5, {y:-100, opacity:0, ease:Back.easeOut}, 0.8)
		.from(".waypoint-7 ul.etapas li.numb3 .ul_text", 0.5, {y:100, opacity:0, ease:Back.easeOut}, 0.8)
	group8
		.from(".waypoint-8 h2", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-8 .franja", 0.5, {opacity:0}, 0.1)
		.from(".waypoint-8 .franja ul li.numb1", 0.5, {x:50, opacity:0, ease:Back.easeOut}, 0.5)
		.from(".waypoint-8 .franja ul li.numb2", 0.5, {x:50, opacity:0, ease:Back.easeOut}, 0.7)
		.from(".waypoint-8 .franja ul li.numb3", 0.5, {x:50, opacity:0, ease:Back.easeOut}, 0.9)
	// group9
	// 	.from(".waypoint-9 h2", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
	// 	.from(".waypoint-9 .left", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 0.5)
	// 	.from(".waypoint-9 .right", 0.5, {x:100, opacity:0, ease:Back.easeOut}, 0.6)
	// group10
	// 	.from(".waypoint-10 h3", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
	// 	.from(".waypoint-10 ul", 0.5, {opacity:0, delay:0.7}, 0.1)
	// 	.from(".waypoint-10 .nav-left", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.5)
	// 	.from(".waypoint-10 .nav-right", 0.5, {x:50, opacity:0, ease:Back.easeOut}, 0.6)
	//TAB 3
	group11
		.from(".waypoint-11 h2", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-11 p", 0.5, {y:100, opacity:0, ease:Back.easeOut}, 0.3)
	group12
		.from(".waypoint-12 h2", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-12 h5", 0.5, {y:-100, opacity:0, ease:Back.easeOut}, 0.2)
		.from(".waypoint-12 ul li.numb1", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-12 ul li.numb1 img", 0.5, {y:-50, opacity:0, ease:Back.easeOut}, 0.2)
		.from(".waypoint-12 ul li.numb2", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 0.4)
		.from(".waypoint-12 ul li.numb2 img", 0.5, {y:-50, opacity:0, ease:Back.easeOut}, 0.5)
		.from(".waypoint-12 ul li.numb3", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 0.7)
		.from(".waypoint-12 ul li.numb3 img", 0.5, {y:-50, opacity:0, ease:Back.easeOut}, 0.8)
		.from(".waypoint-12 ul li.numb4", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 1)
		.from(".waypoint-12 ul li.numb4 img", 0.5, {y:-50, opacity:0, ease:Back.easeOut}, 1.1)
	group13
		.from(".waypoint-13 h2", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-13 .franja", 0.5, {opacity:0}, 0.1)
		.from(".waypoint-13 .franja ul li.numb1", 0.5, {x:50, opacity:0, ease:Back.easeOut}, 0.5)
		.from(".waypoint-13 .franja ul li.numb2", 0.5, {x:50, opacity:0, ease:Back.easeOut}, 0.7)
	// group14
	// 	.from(".waypoint-14 h2", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
	// 	.from(".waypoint-14 .left", 0.5, {x:-100, opacity:0, ease:Back.easeOut}, 0.5)
	// 	.from(".waypoint-14 .right", 0.5, {x:100, opacity:0, ease:Back.easeOut}, 0.6)
	group15
		.from(".waypoint-15 h3", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.1)
		.from(".waypoint-15 ul", 0.5, {opacity:0, delay:0.7}, 0.1)
		.from(".waypoint-15 .nav-left", 0.5, {x:-50, opacity:0, ease:Back.easeOut}, 0.5)
		.from(".waypoint-15 .nav-right", 0.5, {x:50, opacity:0, ease:Back.easeOut}, 0.6)



	iniciar('finanzas');

	// 

	$("ul.menu-tabs li").on("click", function(){

		Waypoint.destroyAll();

		var finanzas = $(this).hasClass('uno');
		var universitaria = $(this).hasClass('dos');
		var agentes = $(this).hasClass('tres');

		if(finanzas){

			$('.section-scholarships').addClass('active');
			$('.sec-financiera.agentes').removeClass('active');
			$('.sec-financiera.section-univ').removeClass('active');

			$('li.one').addClass('on');
			$('li.two').removeClass('on');
			$('li.three').removeClass('on');

			$('li.one .arrow').show();
			$('li.two .arrow').hide();
			$('li.three .arrow').hide();

			group1.pause(0);
			group2.pause(0);
			group3.pause(0);
			group4.pause(0);
			group5.pause(0);

			iniciar('finanzas');

		}else if(agentes){
			
			$('.section-scholarships').removeClass('active');
			$('.sec-financiera.agentes').addClass('active');
			$('.sec-financiera.section-univ').removeClass('active');

			$('li.one').removeClass('on');
			$('li.two').removeClass('on');
			$('li.three').addClass('on');

			$('li.one .arrow').hide();
			$('li.two .arrow').hide();
			$('li.three .arrow').show();

			group6.pause(0);
			group7.pause(0);
			group8.pause(0);
			//group9.pause(0);
			//group10.pause(0);

			iniciar('agentes');

		}else if(universitaria){
			
			$('.section-scholarships').removeClass('active');
			$('.sec-financiera.agentes').removeClass('active');
			$('.sec-financiera.section-univ').addClass('active');

			$('li.one').removeClass('on');
			$('li.two').addClass('on');
			$('li.three').removeClass('on');

			$('li.one .arrow').hide();
			$('li.two .arrow').show();
			$('li.three .arrow').hide();

			group11.pause(0);
			group12.pause(0);
			group13.pause(0);
			//group14.pause(0);
			group15.pause(0);

			iniciar('universitaria');
		};

	});
	
	//NAV FIXED
	var delay = 600;
	var timeout = null;

	$(window).on('scroll', function() {

		var navHeight = $('header').height();

		clearTimeout(timeout);

		if ($(window).scrollTop() > navHeight) {
			$('header').addClass('fixed');
			
			$('header').removeClass("hidden");

			$('header').on('mouseover', function(){
				$('header').addClass('show');
			});
			
			$('header').on('mouseout', function(){
				$('header').removeClass('show');
			});
	
			timeout = setTimeout(function(){
				$('header').addClass("hidden");
			},delay);
		}
		else {
			$('header').removeClass('fixed');
			//$('header').css("visibility", "visible");
			$('header').removeClass("hidden");
			$('.fixed').removeClass("show");
		}	

	});

	$("header .btn-menu-open").on("click", function(){
		$("header nav").addClass('nav-active');
	});

	$("header nav .btn-menu-close").on("click", function(){
		$("header nav").removeClass('nav-active');
		//$('header').removeClass('show');
	});

	// 
	if(location.href.indexOf("type=ahorro") != -1) {

		var cartilla = $('#box-cartilla');
			
		cartilla.fadeIn('slow');


		jQuery.fn.center = function(parent) {
		    if (parent) {
		        parent = this.parent();
		    } else {
		        parent = window;
		    }
		    this.css({
		        "position": "absolute",
		        "top": ((($(parent).height() - this.outerHeight()) / 8) + $(parent).scrollTop() + "px"),
		        "left": ((($(parent).width() - this.outerWidth()) / 5) + $(parent).scrollLeft() + "px")
		    });

			return this;
		}
		
		$('.mensaje').center(true);

		$("body").css("overflow", "hidden");
			
		
		$('#box-cartilla .mensaje .contenedor img').on('click', function(){
			//contacto.removeClass('show');
			cartilla.hide();
			$("body").css("overflow", "initial");
		})

	}










});
//})();