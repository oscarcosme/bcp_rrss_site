var init = (function () {

	//VARIABLES
	var group1 = new TimelineMax({paused:true});
		group2 = new TimelineMax({paused:true});
		group3 = new TimelineMax({paused:true});
		group4 = new TimelineMax({paused:true});
	//Becas y creditos
		group5 = new TimelineMax({paused:true});
		group6 = new TimelineMax({paused:true});
		group7 = new TimelineMax({paused:true});
		//group8 = new TimelineMax({paused:true});
		group8_1 = new TimelineMax({paused:true});
		group9 = new TimelineMax({paused:true});
		group10 = new TimelineMax({paused:true});
		group10_1 = new TimelineMax({paused:true});
		group11 = new TimelineMax({paused:true});
		group12 = new TimelineMax({paused:true});
		group13 = new TimelineMax({paused:true});
		group14 = new TimelineMax({paused:true});
		group15 = new TimelineMax({paused:true});
		group16 = new TimelineMax({paused:true});
		group17 = new TimelineMax({paused:true});
		group18 = new TimelineMax({paused:true});
		group19 = new TimelineMax({paused:true});
	var player;

	//var place = $(".page").attr("data-section");

	//INICIAR ANIMACIONES

	//Becas y creditos
	function iniciar(tab){

		//Destruimos la animación para mobile
		var isMobile = window.matchMedia("screen and (max-width: 760px)");

		switch(tab){
			
			case "becas":

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-6'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group6.play();
				    }else if(direction == 'up'){
				    	group6.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-7'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group7.play();
				    }else if(direction == 'up'){
				    	group7.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-8-1'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group8_1.play();
				    }else if(direction == 'up'){
				    	group8_1.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '50%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-9'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group9.play();
				    }else if(direction == 'up'){
				    	group9.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-10'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group10.play();
				    }else if(direction == 'up'){
				    	group10.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-10-1'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group10_1.play();
				    }else if(direction == 'up'){
				    	group10_1.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '50%'
				});

				break;

			case 'credits':

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-11'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group11.play();
				    	console.log('1');
				    }else if(direction == 'up'){
				    	group11.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-12'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group12.play();
				    	console.log('2');
				    }else if(direction == 'up'){
				    	group12.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-13'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group13.play();
				    }else if(direction == 'up'){
				    	group13.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-14'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group14.play();
				    }else if(direction == 'up'){
				    	group14.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-15'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group15.play();
				    }else if(direction == 'up'){
				    	group15.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-17'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group17.play();
				    }else if(direction == 'up'){
				    	group17.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-18'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group18.play();
				    }else if(direction == 'up'){
				    	group18.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '70%'
				});

				new Waypoint({
				  element: document.getElementsByClassName('waypoint-19'),
				  handler: function(direction) {
				    if(direction == 'down'){
				    	group19.play();
				    }else if(direction == 'up'){
				    	group19.reverse();
				    }
				    if (isMobile.matches) {
				        this.destroy();
				    }
				  },
				  offset: '70%'
				});

				break;
		}
	}


	//ANIMANDO

	group6
		.from("ul.objectives li.1", 0.8, {right: 500, opacity:0, ease:Cubic.easeOut}, 0.1)
		.from("ul.objectives li.2", 0.8, {opacity:0, ease:Cubic.easeOut}, 0.3)
		.from("ul.objectives li.3", 0.8, {opacity:0, ease:Cubic.easeOut}, 0.6)
	group7
		.from(".waypoint-7 h2", 0.5, {opacity:0, x: 50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-7 .one", 0.5, {opacity:0, delay: 0.5, ease:Cubic.easeOut}, 0.1)
		.from(".waypoint-7 .one .top", 1, {opacity:0, x:-100, ease:Cubic.easeOut}, 1)
		.from(".waypoint-7 .one .bottom", 1, {opacity:0, x:-100, ease:Cubic.easeOut}, 1.1)
		.from(".waypoint-7 .two", 0.5, {opacity:0, delay: 0.5, ease:Cubic.easeOut}, 0.3)
		.from(".waypoint-7 .three", 0.5, {opacity:0, delay: 0.5, ease:Cubic.easeOut}, 0.2)
		.from(".waypoint-7 .three .top", 1, {opacity:0, x:100, ease:Cubic.easeOut}, 1.2)
		.from(".waypoint-7 .three .bottom", 1, {opacity:0, x:100, ease:Cubic.easeOut}, 1.3)
	group8_1
		.from(".waypoint-8-1 h2", 0.5, {opacity:0, x: 50, ease:Back.easeOut}, 0.1)
		//.from(".waypoint-8-1 img", 1, {left: '-200%'}, 0.2)
		.from(".waypoint-8-1 .sub-container .two", 0.5, {opacity:0, x:-300, ease:Back.easeOut}, 0.6)
		.from(".waypoint-8-1 .sub-container .one", 0.5, {opacity:0, x:300, ease:Back.easeOut}, 0.7)
		.from(".waypoint-8-1 .sub-container .three", 0.5, {opacity:0, y:-150, ease:Back.easeOut}, 0.8)
		.from(".waypoint-8-1 .sub-container .four", 0.5, {opacity:0, y:150, ease:Back.easeOut}, 0.9)
	group9
		.from(".waypoint-9 .container", 0.5, {opacity:0, delay: 0.2, ease:Cubic.easeOut})
		.from(".waypoint-9 h2", 0.5, {opacity:0, x: 50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-9 .left", 0.5, {opacity:0, x: -200, ease:Back.easeOut}, 0.6)
		.from(".waypoint-9 .right", 0.5, {opacity:0, x: 200, ease:Back.easeOut}, 0.7)
	group10
		.from(".waypoint-10", 0.5, {opacity:0, delay: 0.2, ease:Cubic.easeOut})
	group10_1
		.from(".waypoint-10-1 .nav-left", 0.5, {opacity:0, x:-50, ease:Back.easeOut})
		.from(".waypoint-10-1 .nav-right", 0.5, {opacity:0, x:50, ease:Back.easeOut})

	group11
		.from(".waypoint-11", 0.5, {opacity:0, delay: 0.2, ease:Cubic.easeOut}, 0.5)
		.from(".waypoint-11 .box-title", 0.5, {opacity:0, y:300, ease:Back.easeOut}, 0.6)
		.from(".waypoint-11 ul li.1", 0.8, {opacity:0, x:200, ease:Back.easeOut}, 0.7)
		.from(".waypoint-11 ul li.2", 0.8, {opacity:0, x:200, ease:Back.easeOut}, 1)
		.from(".waypoint-11 ul li.3", 0.8, {opacity:0, x:200, ease:Back.easeOut}, 1.3)
		.from(".waypoint-11 ul li.4", 0.8, {opacity:0, x:200, ease:Back.easeOut}, 1.6)
		.from(".waypoint-11 ul li.5", 0.8, {opacity:0, x:200, ease:Back.easeOut}, 1.9)
	group12
		.from(".waypoint-12 .container", 1, {opacity:0, delay: 0.2, ease:Cubic.easeOut}, 0.1)
		.from(".waypoint-12 h2", 1, {opacity:0, x: -50, ease:Back.easeOut}, 0.2)
	group13
		.from(".waypoint-13 h2", 1, {opacity:0, x: -50, ease:Back.easeOut})
		.from(".section-finance .bquote", 1, {opacity:0, y: 500, ease:Back.easeOut})
	group14
		.from(".waypoint-14 div", 1, {opacity:0, delay:0.5, ease:Back.easeOut})
	group15
		.from(".waypoint-15 h2", 1, {opacity:0, x: -50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-15 p", 1, {opacity:0, y: 100, ease:Back.easeOut}, 0.2)
	group17
		.from(".waypoint-17 h2", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-17 img", 0.5, {opacity:0, y:200, ease:Back.easeOut}, 0.4)
		.from(".waypoint-17 .sub-container .two", 0.5, {opacity:0, x:-300, ease:Back.easeOut}, 0.6)
		.from(".waypoint-17 .sub-container .one", 0.5, {opacity:0, x:300, ease:Back.easeOut}, 0.7)
		.from(".waypoint-17 .sub-container .three", 0.5, {opacity:0, y:-150, ease:Back.easeOut}, 0.8)
		.from(".waypoint-17 .sub-container .four", 0.5, {opacity:0, y:150, ease:Back.easeOut}, 0.9)
	group18
		.from(".waypoint-18 h2", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-18 .left", 0.5, {opacity:0, x:-300, ease:Back.easeOut}, 0.5)
		.from(".waypoint-18 .right", 0.5, {opacity:0, x:300, ease:Back.easeOut}, 0.6)
	group19
		.from(".waypoint-19 h3", 0.5, {opacity:0, y:-50, ease:Back.easeOut}, 0.1)
		.from(".waypoint-19 ul", 0.5, {opacity:0, delay: 0.5, ease:Back.easeOut}, 0.5)
		.from(".waypoint-19 .nav-left", 0.5, {opacity:0, x:-50, ease:Back.easeOut}, 0.8)
		.from(".waypoint-19 .nav-right", 0.5, {opacity:0, x:50, ease:Back.easeOut}, 0.9)


	iniciar('becas');

		$('ul.menu-tabs > li').on("click", function(){
			
			Waypoint.destroyAll();

			var becas = $(this).hasClass('one');
			var creditos = $(this).hasClass('two');


			if (becas){
				
				//$('.section-credits').hide(1000);
				$('.section-scholarships').addClass('active');
				$('.section-credits').removeClass('active');

				$('li.one').addClass("on");
				$('li.two').removeClass("on");
				$('li.two .arrow').hide();
				$('li.one .arrow').show();

				group2.pause(0);
				group3.pause(0);
				group4.pause(0);
				group5.pause(0);
				group6.pause(0);
				group7.pause(0);
				//group8.pause(0);
				group9.pause(0);
				group10.pause(0);
				group10_1.pause(0);

				iniciar('becas');

			}else if(creditos){

				//$('.section-scholarships').hide(1000);
				//$('.section-credits').show(1000);

				$('.section-scholarships').removeClass('active');
				$('.section-credits').addClass('active');

				$('li.two').addClass("on");
				$('li.one').removeClass("on");
				$('li.two .arrow').show();
				$('li.one .arrow').hide();
					
				group11.pause(0);
				group12.pause(0);
				group13.pause(0);
				group14.pause(0);
				group15.pause(0);
				group16.pause(0);
				group17.pause(0);
				group18.pause(0);
				group19.pause(0);

				iniciar('credits');
			};

		});

		if(location.href.indexOf("type=credito") != -1) {

			iniciar('credits');

		}











})();