<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>Document</title>
	<!-- <link rel="stylesheet" href="http://localhost/bcp_rrss_site/css/print.css"> -->
	<link rel="stylesheet" href="http://circusint.com/2015/bcp/bcp_rrss_site/css/print.css">

</head>
<body>
	<table class="datos" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="2">
				<img src="images/bg-logo-bcp.png" alt="">
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center" height="100">
				<span class="title">Calculadora de Crédito de Estudio Patronato BCP</span>
			</td>
		</tr>
		<tr>
			<td class="box">
				<div style="font-family:'flexoBold'">¿Cuántos ciclos te faltan para terminar la carrera?</div>
				<div>*Recuerda que el financiamiento es a partir del 3er ciclo</div>
			</td>
			<td class="box">
				<!-- <input id="ciclo" type="number"> -->
				<span class="var" id="ciclo"><?php echo $_GET['a']; ?></span>
			</td>
		</tr>
		<tr>
			<td class="box">
				<span style="font-family:'flexoBold'">¿Cuánto pagas por ciclo?</span>
			</td>
			<td class="box">
				<!-- <input id="monto" type="number"> -->
				<span class="var" id="monto">S/ <?php echo $_GET['b']; ?></span>
			</td>
		</tr>
		<tr>
			<td class="box">
				<span style="font-family:'flexoBold'">Total a financiar</span> (Cantidad total de tu préstamo)<sup>*</sup>
			</td>
			<td class="box">
				<span class="two">S/ <?php echo number_format($_GET['v']); ?></span>
			</td >
		</tr>
		<tr>
			<td class="box">
				<span class="three" style="font-family:'flexoBold'">¡Esta es tu cuota mensual total!</span>
			</td>
			<td class="box">
				<span class="four">S/ <?php echo $_GET['p']; ?></span>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="box" style="color:#868686;font-size:12px">
				*Debes tener en cuenta que las cuotas son referenciales, sujetas a calificación y a la fecha de desembolso de tu crédito.
			</td>
		</tr>
	</table>
</body>
</html>