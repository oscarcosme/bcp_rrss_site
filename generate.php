<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

	require 'vendor/autoload.php';

	ob_start();
	include 'print.php';
	$codigo = ob_get_clean();

	//var_dump($codigo);
	//exit();
	
	//$codigo = '';


	//reference the Dompdf namespace
	use Dompdf\Dompdf;

	// instantiate and use the dompdf class
	$dompdf = new Dompdf();

	$dompdf->loadHtml(utf8_decode($codigo));

	//$dompdf->set_base_path('http://localhost/bcp_rrss_site/');

	// (Optional) Setup the paper size and orientation
	$dompdf->setPaper('A4');

	$dompdf->set_option('isHtml5ParserEnabled', true);

	// Render the HTML as PDF
	$dompdf->render();

	// Output the generated PDF to Browser
	$dompdf->stream('Credito_de_estudios');

	
	header('Content-type: application/pdf');


?>